﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingSite.Controllers
{
    public class RegistryController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Our Registries";
            ViewBag.SubTitle = "Gifts!";

            return View("Registry");
        }
    }
}
