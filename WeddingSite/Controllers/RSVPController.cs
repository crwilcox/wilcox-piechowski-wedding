﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeddingSite.Models;

namespace WeddingSite.Controllers
{
    public class RSVPController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "RSVP";
            //ViewBag.SubTitle = "Please enter your last and first name as it is on the invitation.";
			ViewBag.SubTitle = "RSVPs are closed.";
            return View();
        }

        // Step 1.  user punches their name in. we fetch their data so they can fill it in for us.
        public ActionResult FindGuest(string lastName, string firstName)
        {
            var rsvp = new RSVPModel(firstName, lastName);

            if (string.IsNullOrWhiteSpace(lastName) || string.IsNullOrWhiteSpace(firstName) || !rsvp.IsUserMatch())
            {
                // without both name fields filled in, we don't want to commit
                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "Please enter your name exactly as on the invitation.  Capitalization is important.";
                return View("Index");
            }
            else
            {
                // Check for existence of last/first pair.  on an exact match, ask if the guest is attending.
                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "Do you plan to attend the festivities?";
                return View("IsGuestAttending", rsvp);
            }
        }

        // Step 2. User sends us their filled information.  We then let them know they are confirmed
        public ActionResult GuestAttending(FormCollection fc)
        {
            string lastName = fc.GetValue("LastName").AttemptedValue;
            string firstName = fc.GetValue("FirstName").AttemptedValue;

            var rsvp = new RSVPModel(firstName, lastName);

            // Check again that we have all fields and a good rsvp object
            if (string.IsNullOrWhiteSpace(lastName) || string.IsNullOrWhiteSpace(firstName) || !rsvp.IsUserMatch())
            {
                // without both name fields filled in, we don't want to commit
                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "Please enter in your First and Last Name exactly as on the invitation.  Capitalization is important.";
                return View("Index");
            }
            else
            {
                // user is attending
                // Check for existence of last/first pair.  on an exact match, present a list to fill in for guests
                // the table should have 
                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "Please enter your meal choice, any guests names, and a song choice (optional) for the festivities...";
                return View("ConfirmGuests", rsvp);
            }
        }

        public ActionResult GuestNotAttending(FormCollection fc)
        {
            string lastName = fc.GetValue("LastName").AttemptedValue;
            string firstName = fc.GetValue("FirstName").AttemptedValue;

            var rsvp = new RSVPModel(firstName, lastName);

            // Check again that we have all fields and a good rsvp object
            if (string.IsNullOrWhiteSpace(lastName) || string.IsNullOrWhiteSpace(firstName) || !rsvp.IsUserMatch())
            {
                // without both name fields filled in, we don't want to commit
                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "Please enter your name exactly as on the invitation.  Capitalization is important.";
                return View("Index");
            }
            else
            {
                // user is not attending.  set everything back to defaults
                foreach(var guest in rsvp.invitation.Guests)
                {
                    guest.Attending = false;
                    guest.AttendingCeremony = false;
                    guest.AttendingDinner = false;
                    guest.AttendingReception = false;
                    guest.DinnerChoice = string.Empty;
                    guest.SongChoice = string.Empty;
                }
              
                rsvp.SaveInvitationToStorage();

                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "Thanks for your confirmation!";
                return View("ThankYouForYourConfirmation", rsvp);
            }
        }


        // Step 3. User sends us their filled information.  We then let them know they are confirmed
        public ActionResult GuestsConfirmed(FormCollection fc)
        {
            // add bit about confirmation
            var rsvp = RSVPModel.CreateModelFromFormCollection(fc);

            // Validation
            // check that any guest that is attending is attending something
            var guest = rsvp.invitation.Guests[0];

            // if for some reason guest said they were attending, but didn't check any boxes...
            if (guest.Attending && !guest.AttendingCeremony && !guest.AttendingDinner && !guest.AttendingReception)
            {
                ViewBag.Title = "RSVP";
                ViewBag.SubTitle = "You must attend at least one event.  Please enter your meal choice, any guests names, and a song choice (optional) for the festivities...";
                return View("ConfirmGuests", rsvp);
            }

            // alright.  so we got past the validation
            rsvp.SaveInvitationToStorage();

            ViewBag.Title = "RSVP";
            ViewBag.SubTitle = "Thanks for your confirmation!";
            return View("ThankYouForYourConfirmation", rsvp);
        }
    }
}
