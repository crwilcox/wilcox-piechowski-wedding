﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingSite.Controllers
{
    public class DoctorWhoController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Doctor WHO?";
            ViewBag.SubTitle = "The first question...";

            return View("DoctorWho");
        }
    }
}
