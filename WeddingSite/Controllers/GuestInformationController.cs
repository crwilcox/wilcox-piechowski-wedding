﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingSite.Controllers
{
    public class GuestInformationController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Guest Information";
            ViewBag.SubTitle = "Madison, WI";

            return View("GuestInformation");
        }
    }
}
