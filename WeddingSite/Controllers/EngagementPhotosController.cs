﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingSite.Controllers
{
    public class EngagementPhotosController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Engagement Photos";
            ViewBag.SubTitle = "Look at us!";

            return View("EngagementPhotos");
        }
    }
}
