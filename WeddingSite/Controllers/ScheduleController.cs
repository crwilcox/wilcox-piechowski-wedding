﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingSite.Controllers
{
    public class ScheduleController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Schedule";
            ViewBag.SubTitle = "So you're going to a wedding...";

            return View("Schedule");
        }
    }
}
