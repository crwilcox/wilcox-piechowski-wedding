﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingSite.Controllers
{
    public class ReceptionController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Reception";
            ViewBag.SubTitle = "Memorial Union";

            return View("Reception");
        }
    }
}
