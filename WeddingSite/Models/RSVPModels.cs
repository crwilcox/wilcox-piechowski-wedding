﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Data.Linq;
using System.Linq;

namespace WeddingSite.Models
{
    public class RSVPModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 1)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        private string AccountName = "wilcoxpiechowskiwedding";
        private string AccountKey = "0x1h7UykX9K3mMvAR+x8J/B5wjL/PUOZlJQJlUiPHDyBFcyrsOagK6x6tySA1Ih9hyH7sXkxMlV02L2T0xBeuQ==";

        public RSVPModel()
        {
            invitation = new Invitation();
        }

        public RSVPModel(string firstName, string lastName)
        {
            invitation = new Invitation();

            FirstName = firstName;
            LastName = lastName;

            // load up date no that we have first last name combo.
            IsUserMatch();
        }

        /// <summary>
        /// If the user matches
        /// </summary>
        /// <returns></returns>
        internal bool IsUserMatch()
        {
            // some guests have tried entering in 'person1firstname and person2firstname' instead of just one.
            var simplifiedFirstName = this.FirstName.Split(' ', ',').First().Trim();

            // Get a reference to the storage account, with authentication credentials
            StorageCredentials credentials = new StorageCredentials(AccountName, AccountKey);
            CloudStorageAccount storageAccount = new CloudStorageAccount(credentials, false);
            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();
            var table = tableClient.GetTableReference("guestlist");

            var tableServiceContext = tableClient.GetTableServiceContext();

            TableQuery<GuestEntity> query = new TableQuery<GuestEntity>().Where(
                TableQuery.CombineFilters(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterCondition("FirstName", QueryComparisons.Equal, simplifiedFirstName),
                        TableOperators.Or,
                        TableQuery.GenerateFilterCondition("FirstName", QueryComparisons.Equal, FirstName)
                    ),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("LastName", QueryComparisons.Equal, LastName)));

            var searchedGuest = table.ExecuteQuery(query).FirstOrDefault();

            if (searchedGuest != null)
            {
                // found the correct group
                // now to get all members of this group
                TableQuery<GuestEntity> query2 = new TableQuery<GuestEntity>().Where(
                TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, searchedGuest.PartitionKey));

                var guests = table.ExecuteQuery(query2);

                if (invitation == null) invitation = new Invitation();
                invitation.Guests = guests.ToList();

                return true;
            }
            else
            {
                return false;
            }
        }

        public Invitation invitation;

        public static RSVPModel CreateModelFromFormCollection(FormCollection fc)
        {
            var rsvp = new RSVPModel();

            var partitionKey = fc.GetValue("PartitionKey").AttemptedValue;

            var count = int.Parse(fc.GetValue("GuestCount").AttemptedValue);

            for (int i = 0; i < count; i++)
            {
                GuestEntity g = new GuestEntity();
                g.PartitionKey = partitionKey;
                g.RowKey = i.ToString();
                g.FirstName = fc.GetValue(i + "-FirstName").AttemptedValue;
                g.LastName = fc.GetValue(i + "-LastName").AttemptedValue;
                g.DinnerChoice = fc.GetValue(i + "-DinnerChoice").AttemptedValue;
                g.SongChoice = fc.GetValue(i + "-SongChoice").AttemptedValue;
                g.Guest = bool.Parse(fc.GetValue(i + "-Guest").AttemptedValue);
                g.Attending = true;
                g.AttendingCeremony = fc.GetValue(i + "-AttendingCeremony").AttemptedValue == "true,false";
                g.AttendingDinner = fc.GetValue(i + "-AttendingDinner").AttemptedValue == "true,false";
                g.AttendingReception = fc.GetValue(i + "-AttendingReception").AttemptedValue == "true,false";

                // if this is the first one, this is the important first name and last name.
                if (i == 0)
                {
                    rsvp.FirstName = g.FirstName;
                    rsvp.LastName = g.LastName;
                }

                rsvp.invitation.Guests.Add(g);
            }

            return rsvp;
        }

        internal void SaveInvitationToStorage()
        {
            // Get a reference to the storage account, with authentication credentials
            StorageCredentials credentials = new StorageCredentials(AccountName, AccountKey);
            CloudStorageAccount storageAccount = new CloudStorageAccount(credentials, false);
            // Create the table client.
            var tableClient = storageAccount.CreateCloudTableClient();
            var table = tableClient.GetTableReference("guestlist");

            var batch = new TableBatchOperation();
            foreach (var guest in invitation.Guests)
            {
                // if there is a name for the guest, they are confirmed at this point
                if (!string.IsNullOrWhiteSpace(guest.FirstName) || !string.IsNullOrWhiteSpace(guest.LastName))
                {
                    guest.Confirmed = true;
                }

                batch.Add(TableOperation.InsertOrReplace(guest));
            }
            table.ExecuteBatch(batch);
        }
    }

    public class Invitation
    {
        public List<GuestEntity> Guests;

        public Invitation()
        {
            Guests = new List<GuestEntity>();
        }
    }

    public class GuestEntity : TableEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DinnerChoice { get; set; }
        public string SongChoice { get; set; }
        public bool Confirmed { get; set; }
        public bool Guest { get; set; }
        public bool Attending { get; set; }
        public bool AttendingCeremony { get; set; }
        public bool AttendingDinner { get; set; }
        public bool AttendingReception { get; set; }

        public GuestEntity()
        {
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.DinnerChoice = string.Empty;
            this.SongChoice = string.Empty;
            this.Confirmed = false;
            this.Guest = false;
            this.Attending = false;
            this.AttendingCeremony = false;
            this.AttendingDinner = false;
            this.AttendingReception = false;
        }
    }
}
